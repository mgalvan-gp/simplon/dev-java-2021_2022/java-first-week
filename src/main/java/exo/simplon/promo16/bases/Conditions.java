package exo.simplon.promo16.bases;

public class Conditions {
    public boolean isPositive(int nmbr) {
        if (nmbr >= 0) {
            return true;
        }
        return false;
    }

    public String skynetIA(String str) {
        if (str == "Hello") {
            return "Hello how are you ?";
        } else if (str == "How are you ?") {
            return "I'm fine thank you";
        } else if (str == "Goodbye") {
            return "Goodbye friend !";
        }
        return "I don't understand";
    }

    public void buy(int age, String product) {
        if (product == "Alcohol" && age < 18) {
            System.out.println("Non tu n'as pas la droit !");
        } else {
            System.out.println("Ok !");
        }
    }

    public int greater(int nmbr1, int nmbr2) {
        if (nmbr1 > nmbr2) {
            return nmbr1;
        }
        return nmbr2;
    }
}
